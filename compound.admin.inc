<?php

/**
 * @file
 * Admin page callbacks for the Compound module.
 */

/**
 * Generates the profile type editing form.
 */
function compound_type_form($form, &$form_state, $compound_type, $op = 'edit') {

  if ($op == 'clone') {
    $compound_type->label .= ' (cloned)';
    $compound_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $compound_type->label,
    '#description' => t('The human-readable name of this compound type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($compound_type->type) ? $compound_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $compound_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'compound_type_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this compound type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $compound_type->description,
    '#description' => t('Describe this compound type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save compound type'),
    '#weight' => 40,
  );

  if (!$compound_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete compound type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('compound_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function compound_type_form_submit(&$form, &$form_state) {
  $compound_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $compound_type->save();
  $form_state['redirect'] = 'admin/structure/compound-types';
}

/**
 * Form API submit callback for the delete button.
 */
function compound_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/compound-types/manage/' . $form_state['compound_type']->type . '/delete';
}
