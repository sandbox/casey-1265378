<?php

/**
 * Implements hook_entity_info().
 */
function compound_entity_info() {

  $info['compound_variant'] = array(
    'label' => t('Compound variant'),
    'module' => 'compound',
    'entity class' => 'CompoundVariantEntity',
    'controller class' => 'CompoundVariantEntityController',
    'base table' => 'compound_variant',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'vid',
      'name' => 'variant',
      'label' => 'label',
      'bundle' => 'type',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(),
    // Enable the entity API's admin UI.
    /*'admin ui' => array(
      'path' => 'admin/structure/compound-types/%',
      //'menu wildcard' => '%compound_type_variant',
      'file' => 'compound.admin.inc',
      'controller class' => 'CompoundVariantEntityUIController',
    ),*/
    'access callback' => 'compound_variant_access',
  );

  return $info;
}

/**
 * Implements hook_entity_info_alter().
 */
function compound_entity_info_alter(&$info) {
  foreach (compound_info_types() as $type) {
    $info['compound_variant']['bundles'][$type->type] = array(
      'label' => $type->label,
      /*'admin' => array(
        'path' => 'admin/structure/compound/variant/manage/%compound_type_variant',
        'real path' => 'admin/structure/compound/variant/manage/' . $type->type,
        'bundle argument' => 4,
        'access arguments' => array('administer profiles'),
      ),*/
    );
  }
}
