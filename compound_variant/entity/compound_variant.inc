<?php

/**
 * Class for compound variant entities.
 */
class CompoundVariantEntity extends Entity {

  public $type;
  public $variant;
  public $label;
  public $description;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'compound_variant');
  }

  /**
   * Returns whether the profile type is locked, thus may not be deleted or renamed.
   *
   * Profile types provided in code are automatically treated as locked, as well
   * as any fixed profile type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
