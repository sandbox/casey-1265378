<?php

/**
 * UI controller.
 */
class CompoundVariantEntityUIController extends EntityDefaultUIController {

  public function __construct($entity_type, $entity_info) {
    parent::__construct($entity_type, $entity_info);
    $this->entityBaseType = 'compound_variant';
  }

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();

    $items[$this->path . '/overview'] = $items[$this->path];
    $items[$this->path . '/overview']['type'] = MENU_LOCAL_TASK;
    unset($items[$this->path]);

    return $items;
  }

  public function hook_forms2() {
    // The overview and the operation form are implemented by the controller,
    // the callback and validation + submit handlers just invoke the controller.
    $forms[$this->entityBaseType . '_overview_form'] = array(
      'callback' => 'entity_ui_overview_form',
      'wrapper_callback' => 'entity_ui_form_defaults',
    );
    $forms[$this->entityBaseType . '_operation_form'] = array(
      'callback' => 'entity_ui_operation_form',
      'wrapper_callback' => 'entity_ui_form_defaults',
    );

    // The entity form (ENTITY_TYPE_form) handles editing, adding and cloning.
    // For that form, the wrapper callback entity_ui_main_form_defaults() gets
    // directly invoked via entity_ui_get_form().
    // If there are bundles though, we use form ids that include the bundle name
    // (ENTITY_TYPE_edit_BUNDLE_NAME_form) to enable per bundle alterations
    // as well as alterations based upon the base form id (ENTITY_TYPE_form).
    if (!(count($this->entityInfo['bundles']) == 1 && isset($this->entityInfo['bundles'][$this->entityType]))) {
      foreach ($this->entityInfo['bundles'] as $bundle => $bundle_info) {
        $forms[$this->entityType . '_edit_' . $bundle . '_form']['callback'] = $this->entityType . '_form';
        // Again the wrapper callback is invoked by entity_ui_get_form() anyway.
      }
    }
    return $forms;
  }
}
