<?php

/**
 * @file
 * Defines a compound field type.
 */


require_once dirname(__FILE__) . '/compound_variant.entity.inc';

/**
 * Implements hook_help().
 */
function compound_help($path, $arg) {
  switch ($path) {
    case 'admin/help#compound':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Compound module allows administrators to define custom compound (fieldable) field types. See the <a href="@field-help">Field module help page</a> for more information about fields.', array('@field-help' => url('admin/help/field'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_forms().
 * All compound variant forms share the same form handler.
 */
function compound_forms() {
  $forms = array();
  foreach (compound_info_types() as $type) {
    $forms['compound_variant_' . $type->type . '_form']['callback'] = 'compound_variant_form';
  }
  return $forms;
}

/**
 * Access callback for the entity API.
 */
function compound_variant_access($op, $type = NULL, $account = NULL) {
  return user_access('administer compound types', $account);
}

/**
 * Menu callback for loading the bundle names.
 */
function compound_type_variant_load($type_variant) {
  list($type, $variant) = explode(':', $type_variant);
  return $variant;
}

/**
 * Menu callback for loading the bundle names.
 */
function compound_variant_load($type, $variant) {
  return compound_get_variants($type);
}

/**
 * Gets an array of all variants for a compound type, keyed by the variant name.
 *
 * @param $type_name
 *   The type to return variants for.
 * @param $variant_name
 *   If set, the variant with the given name is returned.
 *
 * @return CompoundTypeVariant[]
 *   Depending whether $variant_name is given, an array of compound type
 *   variants or a single one.
 */
function compound_get_variants($type_name, $variant_name = NULL) {
  $variants = entity_load('compound_variant', isset($variant_name) ? array($variant_name) : FALSE, array('type' => $type_name));
  return isset($variant_name) ? reset($variants) : $variants;
}
