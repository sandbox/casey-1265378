<?php

/**
 * @file
 * Field module functionality for the Compound module.
 */

/**
 * Implements hook_field_info().
 */
function compound_field_info() {
  $info = array();
  foreach (compound_info_types() as $type) {
    $info[$type->type] = array(
      'label' => t('@label (compound)', array('@label' => $type->label)),
      'description' => $type->description,
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => !empty($type->default_widget) ? $type->default_widget : 'compound_embed',
      'default_formatter' => !empty($type->default_formatter) ? $type->default_formatter : 'compound_embed',
    );
  }
  return $info;
}

/**
 * Implements hook_field_presave().
 *
 * Support saving compound entities in @code $item['entity'] @endcode. This
 * may be used to seamlessly create compound entities during host-entity
 * creation or to save changes to the host entity and its compounds at once.
 */
function compound_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as &$item) {
    // In case the entity has been loaded / created, save it and set the id.
    if (isset($item['entity'])) {
      if (!empty($item['entity']->is_new)) {
        $item['entity']->setHostEntity($entity_type, $entity, LANGUAGE_NONE, FALSE);
      }
      $item['entity']->save(TRUE);
      $item = array('id' => $item['entity']->internalIdentifier());
    }
  }
}

/**
 * Implements hook_field_delete().
 */
function compound_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Also delete all embedded compound entities.
  if ($ids = compound_field_item_to_ids($items)) {
    entity_delete_multiple($field['type'], $ids);
  }
}

/**
 * Get an array of compound IDs stored in the given field items.
 */
function compound_field_item_to_ids($items) {
  $ids = array();
  foreach ($items as $item) {
    if (!empty($item['id'])) {
      $ids[] = $item['id'];
    }
  }
  return $ids;
}

/**
 * Implements hook_field_is_empty().
 */
function compound_field_is_empty($item, $field) {
  if (!empty($item['id'])) {
    return FALSE;
  }
  elseif (isset($item['entity'])) {
    return entity_get_controller($item['entity']->entityType())->isEmpty($item['entity']);
  }
  return TRUE;
}

/**
 * Implements hook_field_formatter_info().
 */
function compound_field_formatter_info() {
  $compound_types = array_keys(compound_info_type_names());
  return array(
    'compound_embed' => array(
      'label' => t('Embedded'),
      'field types' => $compound_types,
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function compound_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'compound_embed':

      foreach ($items as $delta => $item) {
        if ($compound = isset($item['entity']) ? $item['entity'] : compound_load($field['type'], $item['id'])) {
          $element[$delta] = $compound->view();
        }
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function compound_field_widget_info() {
  $compound_types = array_keys(compound_info_type_names());
  return array(
    'compound_hidden' => array(
      'label' => t('Hidden'),
      'field types' => $compound_types,
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
    'compound_embed' => array(
      'label' => t('Embedded'),
      'field types' => $compound_types,
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        //'default value' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function compound_field_widget_form($form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  static $recursion = 0;

  switch ($instance['widget']['type']) {
    case 'compound_hidden':
      return $element;

    case 'compound_embed':
      // If the compound form contains another compound, we might ran into a
      // recursive loop. Prevent that.
      if ($recursion++ > 3) {
        drupal_set_message(t('The compound form has not been embedded to avoid recursive loops.'), 'error');
        return $element;
      }
      $field_parents = $element['#field_parents'];
      $field_name = $element['#field_name'];
      $language = $element['#language'];
      $compound = compound_field_get_entity($items[$delta], $field_name, $field['type']);

      // Nest the compound form in a dedicated parent space, by
      // appending [field_name, langcode, delta] to the current parent space.
      // That way the form values of the compound are separated.
      $parents = array_merge($field_parents, array($field_name, $language, $delta));

      $element += array(
        '#element_validate' => array('compound_field_widget_validate'),
        '#parents' => $parents,
      );

      // Put our entity in the form state, so FAPI callbacks can access it.
      $field_state = field_form_get_state($field_parents, $field_name, $language, $form_state);
      $field_state['entity'][$delta] = $compound;
      field_form_set_state($field_parents, $field_name, $language, $form_state, $field_state);

      field_attach_form($compound->entityType(), $compound, $element, $form_state, $language);

      if (empty($element['#required'])) {
        $element['#after_build'][] = 'compound_field_widget_delay_required_validation';
      }
      $recursion--;
      return $element;
  }
}

/**
 * Gets a compound entity for a given field item.
 *
 * @param $field_name
 *   (optional) If given and there is no entity yet, a new entity object is
 *   created for the given item.
 *
 * @return
 *   The entity object or FALSE.
 */
function compound_field_get_entity(&$item, $field_name = NULL, $entity_type) {
  if (isset($item['id'])) {
    return compound_load($entity_type, $item['id']);
  }
  elseif (!isset($item['entity']) && isset($field_name)) {
    $item['entity'] = entity_create($entity_type, array('field_name' => $field_name));
  }
  return isset($item['entity']) ? $item['entity'] : FALSE;
}

/**
 * FAPI #after_build of an individual compound element to delay the validation of #required.
 */
function compound_field_widget_delay_required_validation(&$element, &$form_state) {
  // If the process_input flag is set, the form and its input is going to be
  // validated. Prevent #required (sub)fields from throwing errors while
  // their non-#required compound is empty.
  if ($form_state['process_input']) {
    _compound_collect_required_elements($element, $element['#compound_required_elements']);
  }
  return $element;
}

/**
 * Helper function to temporarily make #required elements non-required.
 */
function _compound_collect_required_elements(&$element, &$required_elements) {
  // Recurse through all children.
  foreach (element_children($element) as $key) {
    if (isset($element[$key]) && $element[$key]) {
      _compound_collect_required_elements($element[$key], $required_elements);
    }
  }
  if (!empty($element['#required'])) {
    $element['#required'] = FALSE;
    $required_elements[] = &$element;
    $element += array('#pre_render' => array());
    array_unshift($element['#pre_render'], 'compound_field_widget_render_required');
  }
}

/**
 * #pre_render callback that ensures the element is rendered as being required.
 */
function compound_field_widget_render_required($element) {
  $element['#required'] = TRUE;
  return $element;
}

/**
 * FAPI validation of an individual compound element.
 */
function compound_field_widget_validate($element, &$form_state, $complete_form) {
  $instance = field_widget_instance($element, $form_state);
  $field = field_widget_field($element, $form_state);
  $field_parents = $element['#field_parents'];
  $field_name = $element['#field_name'];
  $language = $element['#language'];

  $field_state = field_form_get_state($field_parents, $field_name, $language, $form_state);
  $compound = $field_state['entity'][$element['#delta']];

  // Attach field API validation of the embedded form.
  field_attach_form_validate($compound->entityType(), $compound, $element, $form_state);

  // Now validate required elements if the entity is not empty.
  if (!entity_get_controller($compound->entityType())->isEmpty($compound) && !empty($element['#compound_required_elements'])) {
    foreach ($element['#compound_required_elements'] as &$elements) {

      // Copied from _form_validate().
      if (isset($elements['#needs_validation'])) {
        $is_empty_multiple = (!count($elements['#value']));
        $is_empty_string = (is_string($elements['#value']) && drupal_strlen(trim($elements['#value'])) == 0);
        $is_empty_value = ($elements['#value'] === 0);
        if ($is_empty_multiple || $is_empty_string || $is_empty_value) {
          if (isset($elements['#title'])) {
            form_error($elements, t('!name field is required.', array('!name' => $elements['#title'])));
          }
          else {
            form_error($elements);
          }
        }
      }
    }
  }

  // Only if the form is being submitted, finish the compound entity and
  // prepare it for saving.
  if ($form_state['submitted'] && !form_get_errors()) {

    field_attach_submit($compound->entityType(), $compound, $element, $form_state);
    // Put the compound in $item['entity'], so it is saved with the host entity
    // via hook_field_presave() / field API if it is not empty.
    // @see compound_field_presave()
    $item['entity'] = $compound;
    form_set_value($element, $item, $form_state);
  }
}

/**
 * Implements hook_field_widget_error().
 */
function compound_field_widget_error($element, $error, $form, &$form_state) {
  // @todo - when hook_field_validate() raises an error (none for now),
  // form_error($element, $error['message']);
}
