<?php

/**
 * @file
 * Compound Info API, providing information about available compound types.
 */

/**
 * Returns all compound type definitions.
 *
 * @param $module
 *   Optional
 *
 * @return
 *   An array of compound type definitions, keyed by machine name.
 */
function compound_info_types($module = NULL) {
  $query = db_select('compound_type', 'ct')
    ->fields('ct')
    ->orderBy('ct.type', 'ASC');

  if ($module) {
    $query->condition('ct.module', $module);
  }

  $types = array();
  foreach ($query->execute() as $type) {
    $types[$type->type] = $type;
  }

  return $types;
}

function compound_info_type_names($module = NULL) {
  $query = db_select('compound_type', 'ct')
    ->fields('ct', array('type', 'label'));

  if ($module) {
    $query->condition('ct.module', $module);
  }

  $names = array();
  foreach ($query->execute() as $type) {
    $names[$type->type] = $type->label;
  }
  asort($names);
  return $names;
}
