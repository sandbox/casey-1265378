<?php


/**
 * Implements hook_features_pipe_component_alter() for fields.
 */
function compound_features_pipe_field_alter(&$pipe, $data, $export) {
  // Add the fields of the compound entity to the pipe.
  foreach ($data as $identifier) {
    if (($field = features_field_load($identifier)) && $field['field_config']['type'] == 'compound') {
      $fields = field_info_instances('compound', $field['field_config']['field_name']);
      foreach ($fields as $name => $field) {
        $pipe['field'][] = "{$field['entity_type']}-{$field['bundle']}-{$field['field_name']}";
      }
    }
  }
}
