<?php

/**
 * Implements hook_entity_info().
 */
function compound_entity_info() {
  $info['compound_type'] = array(
    'label' => t('Compound type'),
    'module' => 'compound',
    'entity class' => 'CompoundTypeEntity',
    'controller class' => 'CompoundTypeEntityController',
    'base table' => 'compound_type',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'tid',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'compound_type_access',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/compound-types',
      'file' => 'compound.admin.inc',
      'controller class' => 'CompoundTypeEntityUIController',
    ),
  );

  // Define entity types for compound types belonging tot the Compound module
  // itself. Modules that define their own compound types should handle this
  // themselves, including their database scheme.
  foreach (compound_info_types('compound') as $type) {
    $info[$type->type] = array(
      'label' => $type->label,
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'entity class' => 'CompoundEntity',
      'controller class' => 'CompoundEntityController',
      'base table' => 'compound_data_' . $type->type,
      //'revision table' => 'compound_revision_' . $type->type,
      'compound' => TRUE,
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'id',
        //'revision' => 'revision_id',
      ),
      'bundle keys' => array(
        // The bundle key is altered to "variant" if the Compound variant module
        // is enabled.
        'bundle' => 'type',
      ),
      'bundles' => array(
        $type->type => array(
          'label' => $type->label,
          'admin' => array(
            'path' => 'admin/structure/compound-types/manage/%compound_type',
            'real path' => 'admin/structure/compound-types/manage/' . $type->type,
            'bundle argument' => 4,
            'access arguments' => array('administer compound types'),
          ),
        ),
      ),
      'view modes' => array(),
    );
  }

  return $info;
}
