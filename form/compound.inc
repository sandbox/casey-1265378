<?php

class CompoundForm {

  public function build($form, &$form_state) {


    field_attach_form('compound', $compound, $form, $form_state, $language);

    return $form;
  }

  public function submitSave($form, &$form_state) {
    $compound = entity_ui_form_submit_build_entity($form, $form_state);
    // Save and go back.
    $compound->save();
    $form_state['redirect'] = 'admin/structure/compound-types';
  }

  public function submitDelete($form, &$form_state) {
    $form_state['redirect'] = 'admin/structure/compound-types/manage/' . $form_state['compound_type']->type . '/delete';
  }
}
