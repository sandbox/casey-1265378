<?php

/**
 * Controller class for compound-type entities.
 */
class CompoundTypeEntityController extends EntityAPIControllerExportable {

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();
    try {
      $status = parent::save($entity, $transaction);

      if ($status == SAVED_NEW) {
        // create database tables.
        $schema = compound_type_schema($entity);
        foreach ($schema as $name => $table) {
          db_create_table($name, $table);
        }
        drupal_get_schema(NULL, TRUE);

      }

      return $status;
    }
    catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if ($entities) {
      parent::delete($ids, $transaction);

      foreach ($entities as $id => $entity) {
        // Delete database tables.
        $schema = compound_type_schema($entity);
        foreach ($schema as $name => $table) {
          db_drop_table($name);
        }
      }
    }
  }

  public function resetCache(array $ids = NULL) {
    parent::resetCache($ids);

    // Clear entity type and field type cache so this compound type is
    // correctly exposed.
    entity_info_cache_clear();
    field_cache_clear();
    menu_rebuild();
  }
}
