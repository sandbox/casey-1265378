<?php

/**
 * Class for compound entities.
 */
class CompoundEntity extends Entity {

  protected $fieldInfo, $hostEntity, $hostEntityId, $hostEntityType;
  protected $langcode;

  public $item_id, $field_name;

  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, $entityType);
    $this->fieldInfo = field_info_field($this->field_name);
    if (!$this->fieldInfo /*|| $this->fieldInfo['type'] != 'compound'*/) {
      throw new Exception("Invalid field name given.");
    }
  }

  /**
   * Provides info about the field on the host entity, which embeds this
   * compound item.
   */
  public function fieldInfo() {
    return $this->fieldInfo;
  }

  /**
   * Provides info of the field instance containing the reference to this
   * compound item.
   */
  public function instanceInfo() {
    if ($this->fetchHostDetails()) {
      return field_info_instance($this->hostEntityType(), $this->field_name, $this->hostEntityBundle());
    }
  }

  /**
   * Specifies the default label, which is picked up by label() by default.
   */
  public function defaultLabel() {
    // @todo make configurable.
    if ($instance = $this->instanceInfo()) {
      $field = $this->fieldInfo();
      if ($field['cardinality'] == 1) {
        return $instance['label'];
      }
      elseif ($this->item_id) {
        return t('!instance_label @count', array('!instance_label' => $instance['label'], '@count' => $this->delta() + 1));
      }
      else {
        return t('New !instance_label', array('!instance_label' => $instance['label']));
      }
    }
    return t('Unconnected compound item');
  }

  /**
   * Returns the path used to view the entity.
   */
  public function path() {
    if ($this->item_id) {
      return compound_field_get_path($this->fieldInfo) . '/' . $this->item_id;
    }
  }

  /**
   * Returns the URI as returned by entity_uri().
   */
  public function defaultUri() {
    return array(
      'path' => $this->path(),
    );
  }

  /**
   * Sets the host entity. Only possible during creation of a item.
   */
  public function setHostEntity($entity_type, $entity, $langcode = LANGUAGE_NONE) {
    if (!empty($this->is_new)) {
      $this->hostEntityType = $entity_type;;
      $this->hostEntity = $entity;
      $this->langcode = LANGUAGE_NONE;
      list($this->hostEntityId) = entity_extract_ids($this->hostEntityType, $this->hostEntity);
    }
  }

  /**
   * Returns the host entity, which embeds this compound item.
   */
  public function hostEntity() {
    if ($this->fetchHostDetails()) {
      if (!isset($this->hostEntity)) {
        $result = entity_load($this->hostEntityType, array($this->hostEntityId));
        $this->hostEntity = reset($result);
      }
      return $this->hostEntity;
    }
  }

  /**
   * Returns the entity type of the host entity, which embeds this
   * compound item.
   */
  public function hostEntityType() {
    if ($this->fetchHostDetails()) {
      return $this->hostEntityType;
    }
  }

  /**
   * Returns the id of the host entity, which embeds this compound item.
   */
  public function hostEntityId() {
    if ($this->fetchHostDetails()) {
      return $this->hostEntityId;
    }
  }

  /**
   * Returns the bundle of the host entity, which embeds this compound
   * item.
   */
  public function hostEntityBundle() {
    if ($entity = $this->hostEntity()) {
      list($id, $rev_id, $bundle) = entity_extract_ids($this->hostEntityType, $entity);
      return $bundle;
    }
  }

  protected function fetchHostDetails() {
    if (!isset($this->hostEntityId)) {
      if ($this->item_id) {
        // For saved compounds, query the field data to determine the
        // right host entity.
        $query = new EntityFieldQuery();
        $query->fieldCondition($this->fieldInfo(), 'value', $this->item_id);
        $result = $query->execute();
        list($this->hostEntityType, $data) = each($result);
        $this->hostEntityId = $data ? key($data) : FALSE;
      }
      else {
        // No host entity available yet.
        $this->hostEntityId = FALSE;
      }
    }
    return !empty($this->hostEntityId);
  }

  /**
   * Determines the $delta of the reference pointing to this compound
   * item.
   */
  public function delta() {
    if ($this->item_id && $entity = $this->hostEntity()) {
      foreach ($entity->{$this->field_name} as $lang => &$data) {
        foreach ($data as $delta => $item) {
          if ($item['value'] == $this->item_id) {
            return $delta;
          }
        }
      }
    }
  }

  /**
   * Save the compound item.
   *
   * During creation a host entity has to be specified via the setHostEntity()
   * before this function is invoked. For the link between the entities to be
   * fully established, the host entity object is updated automatically to
   * include a reference on this compound item durign saving.
   *
   * @param $create_link
   *   (internal) If FALSE is passed, creating the link to the host entity
   *   is skipped.
   */
  public function save($create_link = TRUE) {
    $transaction = db_transaction();
    try {
      // Make sure we have a host entity during creation.
      if (!empty($this->is_new) && !(isset($this->hostEntityId) || isset($this->hostEntity))) {
        throw new Exception("Unable to create a compound item without a given host entity.");
      }
      $is_new = !empty($this->is_new);
      $return = entity_get_controller($this->entityType)->save($this, $transaction);

      // Create the link to the host entity.
      $host_entity = $this->hostEntity();
      if ($is_new && $create_link) {
        $host_entity->{$this->field_name}[$this->langcode][] = array('value' => $this->item_id);
      }
      // Always save the host entity, so modules are able to react upon changes
      // to the content of the host and any 'last updated' dates of entities get
      // updated.
      entity_save($this->hostEntityType, $host_entity);
      return $return;
    }
    catch (Exception $e) {
      $transaction->rollback($this->entityType, $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Delete the compound item.
   */
  public function delete() {
    parent::delete();
    // Delete the reference of the host entity.
    if ($this->item_id && $entity = $this->hostEntity()) {
      foreach ($entity->{$this->field_name} as $lang => &$data) {
        foreach ($data as $delta => $item) {
          if ($item['value'] == $this->item_id) {
            unset($data[$delta]);
          }
        }
      }
      entity_save($this->hostEntityType(), $entity);
    }
  }
}
