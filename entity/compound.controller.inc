<?php

/**
 * Controller class for compound entities.
 */
class CompoundEntityController extends EntityAPIController {

  /**
   * Validates if an compound entity can be saved.
   *
   * @param CompoundEntity $entity
   *
   * @return
   *   TRUE if the compound entity can be saved, FALSE otherwise.
   */
  public function validate($entity) {
    return $this->isEmpty($entity);
  }

  /**
   * Checks if an compound entity has no data.
   *
   * @param CompoundEntity $entity
   *
   * @return
   *   TRUE if the compound entity has no data, FALSE otherwise.
   */
  public function isEmpty($entity) {
    $entity_type = $entity->entityType();
    list($id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);
    $field_instances = field_info_instances($entity_type, $bundle);

    foreach ($field_instances as $field_instance) {
      $field_name = $field_instance['field_name'];
      $field = field_info_field($field_name);

      // Determine the list of languages to iterate on.
      $languages = field_available_languages($entity_type, $field);

      foreach ($languages as $langcode) {
        if (!empty($entity->{$field_name}[$langcode])) {
          // If at least one collection-field is not empty; the
          // field-collection-item is not empty.
          foreach ($entity->{$field_name}[$langcode] as $field_item) {
            if (!module_invoke($field['module'], 'field_is_empty', $field_item, $field)) {
              return FALSE;
            }
          }
        }
      }
    }
    return TRUE;
  }
}
