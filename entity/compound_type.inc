<?php

/**
 * Class for compound type entities.
 */
class CompoundTypeEntity extends Entity {

  public $type;
  public $label;
  public $description;

  public $module = 'compound';

  public function __construct($values = array()) {
    parent::__construct($values, 'compound_type');
  }

  /**
   * Returns whether the compound type is locked, thus may not be deleted or renamed.
   *
   * Compound types provided in code are automatically treated as locked, as well
   * as any fixed compound type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
