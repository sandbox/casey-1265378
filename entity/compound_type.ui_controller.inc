<?php

/**
 * UI controller for the compound type entity type.
 */
class CompoundTypeEntityUIController extends EntityDefaultUIController {

  public function overviewTable($conditions = array()) {
    // Set "bundle of" so field management links are added. TODO cleanup
    $this->entityInfo['bundle of'] = 'compound';
    $render = parent::overviewTable($conditions);
    unset($this->entityInfo['bundle of']);

    return $render;
  }
}
