<?php

class CompoundFieldController /*extends FieldBaseController implements FieldControllerInterface*/ {

  public function isEmpty($item) {
    if (!empty($item['value'])) {
      return FALSE;
    }
    elseif (isset($item['entity'])) {
      $compound = $item['entity'];
      $instances = field_info_instances('compound', $this->fieldInfo['field_name']);

      foreach ($instances as $instance) {
        $field_name = $instance['field_name'];
        $field = field_info_field($field_name);

        // Determine the list of languages to iterate on.
        $available_languages = field_available_languages('compound', $field);
        $languages = _field_language_suggestion($available_languages, NULL, $field_name);

        foreach ($languages as $langcode) {
          $items = isset($compound->{$field_name}[$langcode]) ? $compound->{$field_name}[$langcode] : array();
          $function = $field['module'] . '_field_is_empty';
          foreach ($items as $field_item) {
            if (!$function($field_item, $field)) {
              // At least one (sub)field is not empty; the compound-set-item is not empty.
              return FALSE;
            }
          }
        }
      }
    }
    return TRUE;
  }

  public function presave($entity_type, $entity, $langcode, &$items) {
    foreach ($items as &$item) {
      // In case the entity has been loaded / created, save it and set the id.
      if (isset($item['entity'])) {
        $item['entity']->setHostEntity($entity_type, $entity);
        if (entity_get_controller('compound')->save($item['entity'])) {
          $item = array('value' => $item['entity']->cid);
        }
      }
    }
  }

  public function delete($entity_type, $entity, $langcode, &$items) {
    // Also delete all embedded entities.
    if ($ids = compound_field_item_to_ids($items)) {
      entity_delete_multiple('compound', $ids);
    }
  }
}
