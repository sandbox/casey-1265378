<?php

class CompoundEmbedFieldWidget {

  public function widgetForm($form, &$form_state, $langcode, $items, $delta, $element) {
    // If the field-collection item form contains another field-collection,
    // we might ran into a recursive loop. Prevent that.
    if ($recursion++ > 3) {
      drupal_set_message(t('The field-collection item form has not been embedded to avoid recursive loops.'), 'error');
      return $element;
    }
    $field_parents = $element['#field_parents'];
    $field_name = $element['#field_name'];
    $language = $element['#language'];
    $field_collection_item = field_collection_field_get_entity($items[$delta], $field_name);

    // Nest the field-collection-item entity form in a dedicated parent space,
    // by appending [field_name, langcode, delta] to the current parent space.
    // That way the form values of the field-collection item are separated.
    $parents = array_merge($field_parents, array($field_name, $language, $delta));

    $element += array(
      '#element_validate' => array('field_collection_field_widget_embed_validate'),
      '#parents' => $parents,
    );

    // Put our entity in the form state, so FAPI callbacks can access it.
    $field_state = field_form_get_state($field_parents, $field_name, $language, $form_state);
    $field_state['entity'][$delta] = $field_collection_item;
    field_form_set_state($field_parents, $field_name, $language, $form_state, $field_state);

    field_attach_form('field_collection_item', $field_collection_item, $element, $form_state, $language);

    if (empty($element['#required'])) {
      $element['#after_build'][] = 'field_collection_field_widget_embed_delay_required_validation';
    }
    $recursion--;
    return $element;
  }

}
